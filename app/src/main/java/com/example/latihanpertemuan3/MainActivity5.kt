package com.example.latihanpertemuan3

import android.R.layout.simple_list_item_1
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main5)
        val data = listOf("a","b","c","d")
        val ini_list = findViewById<ListView>(R.id.ini_list)
        ini_list.adapter = ArrayAdapter(this, simple_list_item_1, data)
        ini_list.setOnItemClickListener { adapterView, view, position, id ->
            Toast.makeText(this, "Anda memilih: ${data[position]}",
                Toast.LENGTH_SHORT).show()
        }
    }
}